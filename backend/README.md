# Backend

## Goal

The goal is to write a prototype of one of our microservices we have in production.

The role of this microservice is to store pictures coming from our cameras.

In your technical solution, you have to take care about scalability, robustness and extensibility.

## Description

Our camera called Tikee has two objectives (left and right). When the Tikee take a shoot, two photos are generated.

Later these photos are processed by a service to merge left and right photos to create a new stitched photo.
We need to be able to know for each tikee, how many photos it generated.

## First part

### Specifications

The service should consume a json with this format:

```json
{
  "s3_key": "677c082c-3a1a-44d9-874a-20169546c653/123456789/left/my_photo.jpg",
  "resolution": "4096x1862",
  "file_size": 456874,
  "shooting_date": "2021-07-16 11:33:10.592579",
  "metadata": {
    "GPSLatitude": 0.34,
    "GPSLongitude": 0.45,
    "GPSAltitude": 0.78,
    "Camera Model Name": "TIKEE",
    "Make": "ENLAPS"
  }
}
```

You need to create the photo in database according to the format of `s3_key`. Let's focus on it:

- **677c082c-3a1a-44d9-874a-20169546c653** is the tikee's uuid. He's unique.
- **123456789** is the tikee's sequence id.
- **left** is the type of photo. You can have 3 values: **left**, **right** or **stitched**
- **my_photo.jpg** is the photo name.

`resolution` is the photo's resolution. The format is `widthxheight`.\
`file_size` represents the weight in bytes of the photo.\
`shooting_date` is the datetime the photo was taken.\
`metadata` contains multiple various informations such as camera brand, camera model, gps information, etc...

All data in the JSON are mandatory expected `metadata`.

**Sequence in depth**

A tikee can have multiple sequences. A tikee takes left and right photos according to the sequence parameter.

Let's resume this sentence with a simple example : the tikee is identified by an uuid. It can have 1 to N sequences. Each sequences are identified by an id between 1 and N. A sequence can have 0 to N pair of left and right photos inside.

In the example above, you can have this kind of `s3_key` pairs:

```
677c082c-3a1a-44d9-874a-20169546c653/123456789/left/my_photo.jpg
677c082c-3a1a-44d9-874a-20169546c653/123456789/right/my_photo.jpg
677c082c-3a1a-44d9-874a-20169546c653/123456789/left/my_photo1.jpg
677c082c-3a1a-44d9-874a-20169546c653/123456789/right/my_photo1.jpg
677c082c-3a1a-44d9-874a-20169546c653/123456789/left/my_photo2.jpg
677c082c-3a1a-44d9-874a-20169546c653/123456789/right/my_photo2.jpg
677c082c-3a1a-44d9-874a-20169546c653/123456789/left/my_photo3.jpg
677c082c-3a1a-44d9-874a-20169546c653/123456789/right/my_photo3.jpg
```

This means that we have a tikee with a sequence that has taken 4 pairs of pictures: `my_photo`, `my_photo1`, `my_photo2`, `my_photo3`

### Business rules

- Photos must be stored in database according to the tikee uuid and sequence id.
- Each photo from a same sequence must have the same resolution.

### Error management

If an error occur during the json process (validation error, business rules error, etc...) raise an error with the `s3_key` in front.

### Models

We do not give any suggestion about the models for this test. According to the business rules and the specifications, you should be able to modelize data to answer the problem.

## Second part

### Specifications

The second part is related to data you've inserted in the first part.

The service should handle all pairs of left and right photos in a same sequence of a same tikee, and send it to a stitching service (the message's sending must not be implemented, a single message print like `s3_key 677c082c-3a1a-44d9-874a-20169546c653/123456789/left/my_photo.jpg has been sent to the stitching service` is enough).

### Business rules

You must not send photos if:

- A left or a right photo is alone (without his right or left photo associated).
- A stitched photo related to the left and right photo already exists (left, right and stitched photo have always the same name).
