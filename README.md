![](enlaps-logo-blue-icon.svg)

## Technical tests

Welcome to Enlaps technical tests. According to the job you've applied for, please select the appropriate repository.

Fork this repository, commit your solution as it was a normal entreprise project. Do not make any merge request as soon as your test is ready.

Enjoy!
